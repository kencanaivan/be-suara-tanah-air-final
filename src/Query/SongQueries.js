const mongoose = require('mongoose');

const findSongsCoverByOriginalSong = (songId) => ([
  {
    $match:  {
      _id: new mongoose.Types.ObjectId(songId)
    }
  }, {
    '$unwind': {
      'path': '$uploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'uploadsongs', 
      'localField': 'uploadMusic', 
      'foreignField': '_id', 
      'as': 'uploadMusic'
    }
  }, {
    '$unwind': {
      'path': '$uploadMusic', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$lookup': {
      'from': 'users', 
      'localField': 'uploadMusic.uploader._id', 
      'foreignField': '_id', 
      'as': 'uploader'
    }
  }, {
    '$unwind': {
      'path': '$uploader', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$set': {
      'uploadMusic.uploader.username': '$uploader.username'
    }
  }, {
    '$group': {
      '_id': '$_id', 
      'title': {
        '$first': '$title'
      }, 
      'duration': {
        '$first': '$duration'
      }, 
      'origin': {
        '$first': '$origin'
      }, 
      'uploadMusic': {
        '$push': '$uploadMusic'
      }, 
      'writer': {
        '$first': '$writer'
      }
    }
  }
])

const findPopularSongs = () => ([
  {
    '$group': {
      '_id': null, 
      'songs': {
        '$push': '$$ROOT'
      }
    }
  }, {
    '$lookup': {
      'from': 'uploadsongs', 
      'let': {}, 
      'pipeline': [
        {
          '$match': {}
        }
      ], 
      'as': 'uploadSongs'
    }
  }, {
    '$project': {
      'songList': {
        '$concatArrays': [
          '$songs', '$uploadSongs'
        ]
      }
    }
  }, {
    '$unwind': {
      'path': '$songList', 
      'preserveNullAndEmptyArrays': true
    }
  }, {
    '$replaceRoot': {
      'newRoot': '$songList'
    }
  }, {
    '$project': {
      '_id': 1, 
      'title': 1, 
      'totalLikes': {
        '$size': '$likes'
      }, 
      'likes': 1, 
      'duration': 1, 
      'origin': 1, 
      'writer': 1, 
      'history': 1, 
      'lyric': 1, 
      'path': 1, 
      'pathImg': 1, 
      'uploadMusic': 1, 
      'songName': 1, 
      'songCover': 1, 
      'uploader': 1, 
      'reports': 1
    }
  }, {
    '$sort': {
      'totalLikes': -1
    }
  }, {
    '$limit': 5
  }, {
    '$lookup': {
      'from': 'users', 
      'localField': 'uploader._id', 
      'foreignField': '_id', 
      'as': 'uploader'
    }
  },{
    '$unwind': {
      'path': '$uploader', 
      'preserveNullAndEmptyArrays': true
    }
  }
])

module.exports = { findSongsCoverByOriginalSong, findPopularSongs  }