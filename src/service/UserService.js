const User = require('../model/User');
const { findUserLikedSongs, findUserUploadedSongs } = require('../Query/UserQueries')
var bcrypt = require('bcryptjs');
const Joi = require('@hapi/joi');

class UserService {
    static hashUserPassword = async (userId, password) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, async (err, hash) => {
                await User.updateOne(
                    { _id: userId },
                    { password: hash }
                )
            });
        });
    }

    static addLikedSongToUser = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $push: { likedMusic: songId } }
        )
    }

    static removeLikedSongFromUser = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $pull: { likedMusic: songId } }
        )
    }

    static addUploadSongToUser = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $push: { uploadMusic: songId } }
        )
    }

    static removeUploadSongFromUser = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $pull: { uploadMusic: songId, likedUploadMusic: songId } }
        )
    }

    static createUserPlaylist = async (userId, playlistId) => {
        await User.updateOne(
            { _id: userId },
            { $push: { playlists: playlistId } }
        )
    }

    static removePlaylistFromuser = async (userId, playlistId) => {
        await User.updateOne(
            { _id: userId },
            { $pull: { playlists: playlistId } }
        )
    }

    static adminRemoveUploadSongFromUser = async (songId) => {
        await User.updateMany({ $pull: { uploadMusic: songId, likedMusic: songId } })
    }

    static fetchUserLikedSong = (userId) => {
        const query = findUserLikedSongs(userId)
        return User.aggregate(query);
    }

    static addUploadSongLikes = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $push: { likedUploadMusic: songId } }
        )
    }

    static removeUploadSongFromUserLikes = async (userId, songId) => {
        await User.updateOne(
            { _id: userId },
            { $pull: { likedUploadMusic: songId } }
        )
    }

    static fetchUsersUploadSong = (userId) => {
        const query = findUserUploadedSongs(userId)
        return User.aggregate(query)
    }

    static validateDuplicateEmail = async (userEmail) => {
        const query = await User.find({ email: userEmail });
        if (query[0]) {
            return false;
        }
        return true;
    }

    static fetchUserById = async (userId) => {
        const user = await User.findById(userId);
        try {
            return user
        } catch (error) {

        }
    }

    static updateUserByid = async (params) => {
        const { username, id:userId } = params
        const user = await User.findById(userId);
        try {
            user.username = username ? username : user.username;
            user.save();
            return user
        } catch (error) {

        }
    }
}

module.exports = UserService;
