const multer = require('multer');
const UploadSong = require('../model/uploadSong');
const { addUploadSongToUser, removeUploadSongFromUser, adminRemoveUploadSongFromUser, addUploadSongLikes, removeUploadSongFromUserLikes } = require('./UserService')
const { addUploadSongToSongs, removeUploadSongFromSongs, adminRemoveUploadSongFromSong } = require('./SongService')
const { adminRemoveUploadSongFromPlaylist } = require('./PlaylistService');
const { findNewUploadedCoveredSongs } = require('../Query/UploadSongQueries')
const fs = require('fs');

class UploadSongService {

  static uploadSong = async (param) => {
    this.uploadedSongPath = "http://localhost:8080/userUpload/" + this.uploadedSongDate + '-' + param.songName;
    const fileName = param.songName;
    const uploadedSongName = fileName.substring(14, fileName - 4)
    const song = {
      uploader: {
        _id: param.uploader,
        email: param.uploaderEmail
      },
      songCover: {
        _id: param.songCover,
        title: param.songCoverName,
        origin: param.songCoverOrigin
      },
      songName: uploadedSongName,
      path: this.uploadedSongPath
    }
    return song;
  };

  static updateUserAndCoverSongByPath = async (uploader, songId) => {
    const uploadedSong = await UploadSong.find({ "path": this.uploadedSongPath });
    await addUploadSongToUser(uploader, uploadedSong[0].id);
    await addUploadSongToSongs(songId, uploadedSong[0].id);
  }

  static deleteAndUpdateUploadSong = async (uploadSongid) => {
    const uploadedSong = await UploadSong.findById(uploadSongid);
    await removeUploadSongFromUser(uploadedSong.uploader, uploadSongid);
    await removeUploadSongFromSongs(uploadedSong.songCover, uploadSongid);
    await UploadSong.deleteOne({ _id: uploadSongid })
    const uploadedSongPath = uploadedSong.path;
    const dirSongPath = 'public' + uploadedSongPath.substring(21, uploadedSongPath.length);
    fs.unlink(dirSongPath, (err) => {
      if (err) throw err;
    });
  }

  static deleteUploadSongAdmin = async (songId) => {
    try {
      const songToBeDeleted = await UploadSong.findById(songId)
      const { songCover } = songToBeDeleted
      await adminRemoveUploadSongFromUser(songId)
      await adminRemoveUploadSongFromPlaylist(songId)
      await adminRemoveUploadSongFromSong(songCover, songId)
      await UploadSong.deleteOne({ _id: songId })
      return songToBeDeleted;
    } catch (error) {

    }
  }

  static fetchAll = async () => {
    try {
      const songs = await UploadSong.find();
      return songs
    } catch (error) {

    }
  }

  static addLike = async (payload) => {
    try {
        const { userId, songData } = payload;
        const { _id } = songData;
        const song = await UploadSong.findById(_id);
        const currLikes = song.likes
        currLikes.push(userId);
        await addUploadSongLikes(userId, _id);
        song.likes = currLikes;
        song.save()
        return song;
    } catch (error) {
        throw(error)
    }
  }
  
  
  static removeLike = async (params) => {
    const { userId, songId } = params;
    try {
        await removeUploadSongFromUserLikes(userId, songId )
        const likedSong = await UploadSong.findById(songId);
        const { likes } = likedSong;
        const newLikes  = likes.filter( id => id !== userId)
        await UploadSong.findByIdAndUpdate(songId, { likes: newLikes})
        return songId;
    } catch (error) {
        throw(error)
    }
  }
  
  static reportUploadSong = async (songId, payload) => {
    const { userId } = payload;
    try {
      const reportedSong = await UploadSong.findById(songId)
      const { reports } = reportedSong
      reports.push(userId)
      reportedSong.save()
      return reportedSong;
    } catch (error) {
      
    }
  }
  
  static fetchUploadSongsWithLimitedReports = () => {
    try {
      const songs = UploadSong.find({'reports.6': {$exists: true}})
      return songs
    } catch (error) {
      
    }
  }

  static fetchNewUploadedCoveredSongs = () => {
    const query = findNewUploadedCoveredSongs()
    return UploadSong.aggregate(query)
   
  }
  
}

UploadSongService.uploadedSongDate = Date.now();
UploadSongService.uploadedSongPath;
UploadSongService.storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/userUpload')
  },
  filename: function (req, file, cb) {
    cb(null, UploadSongService.uploadedSongDate + '-' + file.originalname)
  }
});
UploadSongService.upload = multer({ storage: UploadSongService.storage }).single('file');


module.exports = UploadSongService;
