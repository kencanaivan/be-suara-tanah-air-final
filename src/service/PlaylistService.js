const Playlist = require('../model/Playlist');
const { createUserPlaylist, removePlaylistFromuser } = require('./UserService')
const { findPlaylistSongs } = require('../Query/PlaylistQueries')
const multer = require('multer');


class PlaylistService {

    static create = async (payload) => {
        try {
            const { userId, title } = payload
            const imgPath = "http://localhost:8080/images/defaultPlaylist.svg"
            const playlist = new Playlist ({title})
            playlist.imgPath = imgPath;
            playlist.creator = userId
            await createUserPlaylist(userId, playlist._id)
            playlist.save()
            return playlist
        } catch (error) {
            throw (error)
        }
    }
    
    static getByid = async (id) => {
        const fetchedPlaylist = Playlist.findById(id)
        return fetchedPlaylist;
    }
    
    static updateById = async (playlistId, payload) => {
        try {
            const { title } = payload
            const playlist = await Playlist.findById(playlistId)
            playlist.title = title ? title : playlist.title
            playlist.save()
            return playlist
        } catch (error) {
    
        }
    }
    
    static removeSongFromPlaylist = async (params) => {
        try {
            const { playlistId, songId } = params
            await Playlist.findByIdAndUpdate(playlistId, { $pull: { playlistSongs: songId } })
            return newPlaylistSongs;
        } catch (error) {
    
        }
    }
    

    
    static updateImagePathName = async (playlistId, imageName) => {
        try {
            await Playlist.updateOne(
                {_id: playlistId },
                {imgPath: 'http://localhost:8080/images/' + this.uploadedImgDate +'-'+ imageName}
            )
            return this.uploadedImgDate  +'-'+ imageName;
        } catch (error) {

        }
    };
    
    static addToPlaylist = async (params, payload) => {
        const { playlistId } = params
        const { _id:songId } = payload
        const playlist = await Playlist.findById(playlistId)
        try {
            const { playlistSongs } = playlist;
            for(let i=0; i < playlistSongs.length; i++){
                if(playlistSongs[i] == songId){
                    return json({errMsg: 'song already exists in playlist'})
                }
            }
            playlistSongs.unshift(songId)
            playlist.save()
            return playlist
        } catch (error) {
            
        }
    }
    
    static deletePlaylist = async (params) => {
        const { userId, playlistId } = params
        const playlist = Playlist.findById(playlistId)
        try {
            await removePlaylistFromuser(userId, playlistId)
            await Playlist.remove({_id: playlistId});
            return playlist;
        } catch (error) {
            
        }
    }
    
    static adminRemoveUploadSongFromPlaylist = async (songId) => {
        await Playlist.updateMany({$pull: { playlistSongs: songId }})
    }
    
    static fetchPlaylistSongs = (playlistId) => {
        const query = findPlaylistSongs(playlistId)
        return Playlist.aggregate(query);
    }
}


PlaylistService.uploadedImgDate = Date.now();

PlaylistService.storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images')
    },
    filename: function (req, file, cb) {
        cb(null, PlaylistService.uploadedImgDate + '-' + file.originalname)
    }

})

PlaylistService.upload = multer({ storage: PlaylistService.storage }).single('file')

module.exports = PlaylistService;