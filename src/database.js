const mongoose = require('mongoose');

const connect = () => {
  mongoose.connect('mongodb://localhost:27017/lagu', { useNewUrlParser: true, useUnifiedTopology: true });
  // mongoose.connect('mongodb+srv://ansafa:ansafa123@cluster0.wudd3.mongodb.net/suaraTanahAir?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
};

module.exports = connect;
