const { Router } = require("express");
const requestBodyValidator = require('./middleware/requestBodyValidator');
const registerSchema = require('./model/UserRegisterSchema')
const {
  registerUser,
  getUserById,
  updateUser,
  getUserLikedSongs,
  getUsersUploadSong,
} = require("./controller/UserController");
const { login } = require("./controller/AuthController");
const {
  getSong,
  getSongDetail,
  addLikeSong,
  unlikeSong,
  downloadSong,
  getSongsCoverByOriginalSong,
  getNewUploadedSongList,
  getPopularSongs
} = require("./controller/songController");
const {
  createPlaylist,
  getPlaylistById,
  updatePlaylist,
  deleteSongFromPlaylist,
  addSongToPlaylist,
  deleteUserPlaylist,
  playlistUploadImage,
  updatePlaylistImgPath,
  getPlaylistSongs,
} = require("./controller/PlaylistController");
const {
  adminDeleteUploadSong,
  getAllSongUpload,
  postUploadSong,
  uploadFile,
  deleteUploadSong,
  addLikeUploadSong,
  unlikeUploadSong,
  addReportUploadSong,
  getUploadSongsWithLimitedReports,
  getNewUploadedCoveredSong
} = require("./controller/uploadSongController");

const router = Router();



router.route("/users").post(requestBodyValidator(registerSchema), registerUser);

router.route("/login").post(login);

router.route("/list").post(getSong);

router.route('/uploadSong/newUploaded').get(getNewUploadedCoveredSong);
router.route('/list/newUploaded').get(getNewUploadedSongList);
router.route('/songList/popular').get(getPopularSongs)

router.route("/songpage/:id").get(getSongDetail);

router.route("/userProfile/:userId/update").put(updateUser);
router.route("/userProfile/:userId").get(getUserById);
router.route("/userProfile/:userId/likedSongs").get(getUserLikedSongs);
router
  .route("/userProfile/:userId/playlist/:playlistId")
  .delete(deleteUserPlaylist);

router.route("/list/:songId/likes/:userId").post(addLikeSong);
router.route("/list/:songId/likes/:userId").delete(unlikeSong);
router.route("/list/:songId/songUploaded").get(getSongsCoverByOriginalSong);

router.route("/playlist").post(createPlaylist);
router.route("/playlist/:id").get(getPlaylistById);
router.route("/playlist/:playlistId/songs").get(getPlaylistSongs);
router.route("/playlist/:id").put(updatePlaylist);
router
  .route("/playlist/:playlistId/song/:songId")
  .delete(deleteSongFromPlaylist);
router.route("/playlist/:playlistId/song/:songId").post(addSongToPlaylist);


router.route("/playlist/uploadfile").post(playlistUploadImage);
router.route("/playlist/uploadfile/data").post(updatePlaylistImgPath);

router.route("/song/uploadFile").post(uploadFile);
router.route("/song/uploadFile/:id").delete(deleteUploadSong);
router.route("/song/uploadFile/data").post(postUploadSong);

router.route("/userProfile/:userId/uploadSong").get(getUsersUploadSong);
router.route("/uploadSong/:songId/likes/:userId").post(addLikeUploadSong);
router.route("/uploadSong/:songId/likes/:userId").delete(unlikeUploadSong);
router.route("/uploadSong/:songId/report").post(addReportUploadSong);

// router.route('/song/uploadFileSong/popular').get(getPopularSongs)
router.route('/admin/uploadSongs/:songId').delete(adminDeleteUploadSong)
router.route('/admin/uploadSongs').get(getAllSongUpload)
router.route("/admin/uploadSongs/reports").get(getUploadSongsWithLimitedReports)

router.route("/song/download/:id").get(downloadSong);

module.exports = router;
