const mongoose = require('mongoose')

const playlistSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    imgPath: {
        type: String
    },
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    playlistSongs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'songs'
    }],
    imgPath: {
        type: String
    }
});

const Playlist = mongoose.model('playlists', playlistSchema);

module.exports = Playlist;