const mongoose = require('mongoose')

const songSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    origin: {
        type: String,
        required: true
    },
    writer: {
        type: String,
        required: true,
        default: 'Unknown'       
    },
    history: {
        type: String,
        required: true
    },
    lyric: {
        type: String,
        required: true
    },
    path: {
        type: String
    },
    pathInstrumental: {
        type: String
    },
    pathImg: {
        type: String
    },
    likes: {
        type: Array,
        default: []
    },
    uploadMusic: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'uploadSongs'
    }]
});

const Song = mongoose.model('songs', songSchema);

module.exports = Song;
