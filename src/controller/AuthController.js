const User = require('../model/User');
var bcrypt = require('bcryptjs');

class AuthController {

  static login = async (req, res) => {
    var param = req.body;
    const loggedInUser = await User.find({ email: param.email }, function (err) {
      
    });
    if (loggedInUser[0]) {
      bcrypt.compare(param.password, loggedInUser[0].password, (err, hashRes) => {
        if (hashRes === true) {
          loggedInUser[0].password = '';
          return res.status(200).json({ user: loggedInUser, msg: "user fetched" });
        } else {
          return res.status(200).json({ msg: "wrong password" })
        }
      })
    } else return res.status(200).json({ msg: "user not found" });
  };

}

module.exports = AuthController ;